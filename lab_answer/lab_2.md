1. Apakah perbedaan antara JSON dan XML?
- JSON
It is known as JavaScript Object Notation. It is built using the JavaScript programming language. It's a method of representing things. There is no support for namespaces. It accepts arrays. When compared to XML, its files are more easier to understand. It only supports UTF-8 encoding. is a lightweight data-interchange format that is language agnostic. It is simple to comprehend and produce since it is built on the JavaScript programming language.

- XML
was created to transport data rather than show data It is a suggestion from the World Wide Web Consortium. Extensible Markup Language (XML) is a markup language that specifies a set of rules for encoding texts in a machine-readable and human-readable manner. XML's design objectives are focused on simplicity, universality, and Internet usability. It is a textual data format with excellent Unicode support for many human languages. Although the language's architecture is centred on documents, it is frequently used to describe arbitrary data structures such as those used in online services. It stands for Extensible Markup Language. It is based on SGML. It is a markup language that represents data objects using a tag structure. Namespaces are supported. It is not array-compatible. Its papers are rather tough to read and understand. It includes start and end tags. It has a higher level of security than JSON. It accepts comments. It supports a variety of encodings.

Source : - https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
- HTML
Is a kind of markup language. Is unaffected by case. It also serves as a presentation language. It has its own set of preset tags. Closing tags are not always required. White spaces are not conserved. Displays the design of a web page as it appears on the client side. It is used to show data. Nature is static. Provides native support. The absence of a value is recognised natively. Text parsing does not need any additional application code.

- XML
Is a standard markup language that serves as the foundation for various markup languages. its case sensitive. Is neither a presentation nor a programming language. Tags are established based on the requirements of the coder. XML is adaptable since tags may be defined as required. Closing tags must be used at all times. Capable of maintaining white space. Allows data from databases and associated applications to be transported. It is used to transmit data. Nature is dynamic. Objects are represented by conventions using elements and attributes. In an XML instance document, Xsi:nil on elements is required. To translate text back into JavaScript objects, XML DOM application and implementation code is required.

in other words HTML and XML are linked, with HTML displaying data and describing the structure of a website, and XML storing and transferring data. HTML is a preset language that is basic, while XML is a standard language that specifies additional languages.

Source : - https://www.upgrad.com/blog/html-vs-xml/