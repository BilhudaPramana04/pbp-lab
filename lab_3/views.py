from django.shortcuts import render,redirect
from .forms import friendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
# making it login required to add
@login_required(login_url="/admin/login") 
def index(request):
    friendForm = Friend.objects.all()  # TODO Implement this
    response = {'friends': friendForm}
    return render(request, 'lab3_index.html', response)

# making it login required to add
@login_required(login_url="/admin/login")
def add_friends(request):
    context ={}
  
    # create object of form
    form = friendForm(request.POST, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        # save the form and redirect back to home page
        return redirect('/lab-3')
    
    # else if not valid back to forms again
    else:
        form = friendForm()
    
    context['form']= form
    return render(request, "lab3_form.html", context)