import 'package:flutter/material.dart';
import 'package:lab_6/screens/login.dart';
import 'package:lab_6/screens/registration.dart';
import './aboutme.dart'; //importing the aboutme.dart file
import './contacts.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  // create a field that holds the title of the app called Lab_6
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // make the tittle bold and bigger change the font color
        title: Text(
          'HEYDOC',
          style: TextStyle(
            fontWeight: FontWeight.bold,fontFamily: 'Dosis',color: Color.fromRGBO(34, 40, 49,1),
            fontSize: 30,
          ),
        ),
        backgroundColor: Color.fromRGBO(240, 84, 84,1),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Text(
                'HEYDOC App List',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Dosis',
                  color: Color.fromRGBO(34, 40, 49,1),
                ),
              ),
              decoration: BoxDecoration(
                color: Color.fromRGBO(240, 84, 84,1),
              ),
            ),
            // first ListTile about me
            ListTile(
              title: Text('Covid Drug Recommendations'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CovidDrugRecommendations(),
                  ),
                );
              },
            ),
            // second ListTile contact me
            ListTile(
              title: Text('Contact Me'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Contacts(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Welcome To HEYDOC',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Dosis',
                  color: Color.fromRGBO(34, 40, 49,1)),
            ),
            Text(
              'Made By Billy',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontFamily: 'Dosis',
                color: Color.fromRGBO(34, 40, 49,1),
              ),
            ),
            // make the two buttons horizontal and make them bigger and bold and change the color of the text and the background color of the button and make the space between the text and the button apart
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Dosis',
                      color: Color.fromRGBO(34, 40, 49,1),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Logins(),
                      ),
                    );
                  },
                  color: Color.fromRGBO(240, 84, 84,1),
                ),
                SizedBox(
                  width: 20,
                ),
                RaisedButton(
                  child: Text(
                    'Register',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Dosis',
                      color: Color.fromRGBO(34, 40, 49,1),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Registrations(),
                      ),
                    );
                  },
                  color: Color.fromRGBO(240, 84, 84,1),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
