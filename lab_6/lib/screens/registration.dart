import 'package:flutter/material.dart';

class Registrations extends StatelessWidget {
  const Registrations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register Page',style: TextStyle(color: Color.fromRGBO(34, 40, 49,1)),),
        backgroundColor: Color.fromRGBO(240, 84, 84,1),
      ),
      body: Container(  
        // make the padding at the center of the screen
        padding: EdgeInsets.all(20),
        alignment: Alignment.center,
        child: Text("REGISTER HEREEEEE",style: TextStyle(fontSize: 30,color: Colors.black),),
      ),
    );
  }
}
