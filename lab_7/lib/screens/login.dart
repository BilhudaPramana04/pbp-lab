import 'package:flutter/material.dart';
import './homepage.dart'; 
class Logins extends StatelessWidget {
  const Logins({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  final _formKey = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'LOGIN',
          style: TextStyle(
            fontWeight: FontWeight.bold,fontFamily: 'Dosis',color: Color.fromRGBO(34, 40, 49,1),
            fontSize: 30,
          ),
        ),
        backgroundColor: Color.fromRGBO(240, 84, 84,1),
      ),
      body: Form(
				key: _formKey,
				child: Column(
					children: <Widget> [
						Container (
							decoration: BoxDecoration(
								color: Color.fromRGBO(240, 84, 84,1),
								borderRadius: BorderRadius.circular(20),
							),

							margin: const EdgeInsets.only(top: 100),
							padding: const EdgeInsets.fromLTRB(68,18,68,18),
							child: const Text("Login Here!",
								style: TextStyle(
									fontWeight: FontWeight.bold,
									fontSize: 20,
									color: Color.fromRGBO(34, 40, 49,1),
								),
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Username",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter username!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Password",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter password!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 15),

						SizedBox(
							width: 100,
							height: 40,
							child: ElevatedButton(
								onPressed: () {
									if (_formKey.currentState!.validate()) {
										Navigator.push(
											context,
											MaterialPageRoute(builder: (context) => const MyHomePage()),
										);
									}
								},
								child: const Text("Login",
									style: TextStyle(
										fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(34, 40, 49,1),
                    fontFamily: 'Dosis',
                    fontSize: 20,
									),
								),
                style: ElevatedButton.styleFrom(
                                    primary: Color.fromRGBO(240, 84, 84,1),
                                ),
							),
						),
					],
				),
			),
    );
  }
}