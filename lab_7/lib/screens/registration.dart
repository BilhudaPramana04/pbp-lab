import 'package:flutter/material.dart'; 
import 'package:lab_7/screens/homepage.dart'; // import the homepage

class Registrations extends StatelessWidget { // create a stateless widget
  const Registrations({Key? key}) : super(key: key); // create a constructor

  @override
  Widget build(BuildContext context) { // create a build method
    final _formKey = GlobalKey<FormState>(); // create a form key
    return Scaffold( // create a scaffold
      appBar: AppBar( // create an app bar
        title: Text(
          'REGISTER',
          style: TextStyle(
            fontWeight: FontWeight.bold,fontFamily: 'Dosis',color: Color.fromRGBO(34, 40, 49,1),
            fontSize: 30,
          ),
        ),
        backgroundColor: Color.fromRGBO(240, 84, 84,1),
      ),
      body: Form(
				key: _formKey,
				child: Column(
					children: <Widget> [
						Container (
							decoration: BoxDecoration(
								color: Color.fromRGBO(240, 84, 84,1),
								borderRadius: BorderRadius.circular(20),
							),

							margin: const EdgeInsets.only(top: 100),
							padding: const EdgeInsets.fromLTRB(68,18,68,18),
							child: const Text("Register Now!",
								style: TextStyle(
									fontWeight: FontWeight.bold,
									fontSize: 20,
									color: Color.fromRGBO(34, 40, 49,1),
								),
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Email",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter A Valid Email!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Username",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter A Username!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),
						
            Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Password",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter A Password!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 15),
						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Confirm Your Password",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Confirm Your Password!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),            

						SizedBox(
							width: 150,
							height: 40,
							child: ElevatedButton(
								onPressed: () {
									if (_formKey.currentState!.validate()) {
										Navigator.push(
											context,
											MaterialPageRoute(builder: (context) => const MyHomePage()),
										);
									}
								},
								child: const Text("Register",
									style: TextStyle(
										fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(34, 40, 49,1),
                    fontFamily: 'Dosis',
                    fontSize: 20,
									),
								),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromRGBO(240, 84, 84,1),
                ),
							),
						),
					],
				),
			),
    );
  }
}
