import 'package:flutter/material.dart';

class Contacts extends StatelessWidget {
  const Contacts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'CONTACTS',
          style: TextStyle(
            fontWeight: FontWeight.bold,fontFamily: 'Dosis',color: Color.fromRGBO(34, 40, 49,1),
            fontSize: 30,
          ),
        ),
        backgroundColor: Color.fromRGBO(240, 84, 84,1),
      ),
      body: Container(  
        // make the padding at the center of the screen
        padding: EdgeInsets.all(20),
        alignment: Alignment.center,
        child: Text("You can contact me by Discord Lordofbilly#7750 or line",style: TextStyle(fontSize: 30,color: Colors.black),),
      ),
    );
  }
}
