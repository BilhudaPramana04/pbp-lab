import 'package:flutter/material.dart';
import 'package:lab_7/screens/homepage.dart';
import './screens/homepage.dart';  //importing the contacts.dart file
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Lab_6',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Color.fromRGBO(221, 221, 221,1),
      ),
      home: const MyHomePage(),
    );
  }
}
