# Create your views here.
from django.shortcuts import render,redirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    response = {'data':Note.objects.all()}
    return render(request, 'lab4_index.html',response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        # save the form and redirect back to home page
        return redirect('/lab-4')
    
    # else if not valid back to forms again
    else:
        form = NoteForm()
    
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    response = {'data_note_list':Note.objects.all()}
    return render(request, 'lab4_note_list.html',response)    