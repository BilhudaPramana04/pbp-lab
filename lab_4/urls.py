from django.urls import path
from .views import index,add_note,note_list
# Create file `lab_2/urls.py` with route `''` for `index` path so that you can access the result by accessing [http://localhost:8000/lab-2](http://localhost:8000/lab-2)
urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name = 'add-note' ),
    path('note-list/', note_list, name = 'note-list')
]